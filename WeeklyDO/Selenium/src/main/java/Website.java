import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Website implements WebsiteInterface{

    private WebDriver driver;

    private String homepage;
    private String logoutPage;
    private String loginPage;
    private String registerPage;
    private String tasksPage;
    private String settingsPage;
    private String listsPage;
    public Website(WebDriver driver,String homepage){
        this.driver = driver;
        this.homepage = homepage;
        this.loginPage = homepage + "/login";
        this.registerPage = homepage + "/register";
        this.settingsPage = homepage + "/settings";
        this.logoutPage = homepage + "/logout";
        this.tasksPage = homepage + "/tasks";
        this.listsPage = homepage + "/lists";
    }

    @Override
    public String getHomepage() {
        return this.homepage;
    }

    @Override
    public String getLogoutPage() {
        return this.logoutPage;
    }

    @Override
    public String getRegisterPage() {
        return this.registerPage;
    }

    @Override
    public String getLoginPage() {
        return this.loginPage;
    }

    @Override
    public String getSettingsPage() {
        return this.settingsPage;
    }

    @Override
    public String getTasksPage() {
        return this.tasksPage;
    }

    @Override
    public String getListsPage() {
        return listsPage;
    }

    @Override
    public WebElement getRegisterEmailField() {
        return driver.findElement(By.id("email"));
    }

    @Override
    public WebElement getRegisterPasswordField() {
        return driver.findElement(By.id("password"));
    }

    @Override
    public WebElement getRegisterConfirmPasswordField() {
        return driver.findElement(By.xpath("/html/body/main/div/form/input[4]"));
    }

    @Override
    public WebElement getRegisterRegisterButton() {
        return driver.findElement(By.xpath("/html/body/main/div/form/input[5]"));
    }

    @Override
    public WebElement getRegisterLoginButton() {
        return driver.findElement(By.xpath("/html/body/main/div/form/input[6]"));
    }

    @Override
    public WebElement getLoginEmailField() {
        return driver.findElement(By.id("email"));
    }

    @Override
    public WebElement getLoginPasswordField() {
        return driver.findElement(By.name("password"));
    }

    @Override
    public WebElement getLoginLoginButton() {
        return driver.findElement(By.xpath("/html/body/main/div/form/input[4]"));
    }

    @Override
    public WebElement getLoginRegisterButton() {
        return driver.findElement(By.xpath("/html/body/main/div/form/input[5]"));
    }

    @Override
    public WebElement getSidebarTasksLink() {
        return driver.findElement(By.xpath("/html/body/main/aside/a[1]"));
    }

    @Override
    public WebElement getSidebarListsLink() {
        return driver.findElement(By.xpath("/html/body/main/aside/a[2]"));
    }

    @Override
    public WebElement getSidebarSettingsLink() {
        return driver.findElement(By.xpath("/html/body/main/aside/a[3]"));
    }

    @Override
    public WebElement getSidebarLogoutLink() {
        return driver.findElement(By.xpath("/html/body/main/aside/a[4]"));
    }

    @Override
    public WebElement getTasksTopBarWeekBack() {
        return driver.findElement(By.xpath("/html/body/main/div[1]/span[1]"));
    }

    @Override
    public WebElement getTasksTopBarWeekForward() {
        return driver.findElement(By.xpath("/html/body/main/div[1]/span[4]"));
    }

    @Override
    public String getTasksTopBarWeekStartDate() {
        return driver.findElement(By.xpath("//*[@id=\"first-date\"]")).getText();
    }

    @Override
    public String getTasksTopBarWeekEndDate() {
        return driver.findElement(By.xpath("//*[@id=\"last-date\"]")).getText();
    }

    @Override
    public List<WebElement> getTasksDaysForWeek() {
        return driver.findElements(By.cssSelector("body > main > div.main-content > *"));
    }

    @Override
    public String getTasksTaskDate(WebElement day) {
        return day.findElement(By.className("date")).getText();
    }

    @Override
    public WebElement getTasksNewTaskIcon(WebElement day) {
        return day.findElement(By.className("itemplus"));
    }

    @Override
    public List<WebElement> getTasksDaysTasks(WebElement day) {
        List<WebElement> result = day.findElements(By.cssSelector("*"));
        for (int x=0;x < 3;x++){
            result.remove(0);
        }

        return result;
    }

    @Override
    public WebElement getTasksTaskTitleField(WebElement task) {
        return task.findElement(By.xpath("//h4/span"));
    }



    /*
     * !!!!!!!DOES NOT WORK!!!!!!!!!!!!!!!!!
     * Element not intractable
     * */


    @Override
    public WebElement getTasksTaskDescriptionField(WebElement task) {
        return task.findElement(By.xpath("//div"));
    }

    @Override
    public boolean getTasksTaskClosedStatus(WebElement task) {
        System.out.println(task.getAttribute("class"));
        return task.getAttribute("class").contains("closed");
    }

    @Override
    public WebElement getTasksTaskCloseIcon(WebElement task) {
        return task.findElement(By.xpath("//h4/img[1]"));
    }

    @Override
    public WebElement getTasksTaskDeleteIcon(WebElement task) {
        return task.findElement(By.xpath("//h4/img[2]"));
    }

    @Override
    public WebElement getListsNewListIcon() {
        return driver.findElement(By.xpath("/html/body/main/div[2]/img"));
    }

    @Override
    public List<WebElement> getListsAllLists() {
        List<WebElement> elements = driver.findElements(By.cssSelector("body > main > div.main-content > *"));
        elements.remove(0);
        return elements;
    }


    /*
    * !!!!!!!DOES NOT WORK!!!!!!!!!!!!!!!!!
    *   findElement returns null
    * */

    @Override
    public WebElement getListsListNameField(WebElement list) {
        return list.findElement(By.xpath("//h3/span"));
    }

    @Override
    public WebElement getListsDeleteListIcon(WebElement list) {
        return list.findElement(By.xpath("//h3/img[1]"));
    }

    @Override
    public WebElement getListsAddListItemIcon(WebElement list) {
        return list.findElement(By.xpath("//h3/img[2]"));
    }

    @Override
    public List<WebElement> getListsAllListElements(WebElement list) {
        return list.findElements(By.className("item"));
    }

    @Override
    public WebElement getListElementTitleField(WebElement listElement) {
        return listElement.findElement(By.xpath("//h4/span"));
    }

    /*
     * !!!!!!!DOES NOT WORK!!!!!!!!!!!!!!!!!
     * Element not intractable
     * */

    @Override
    public WebElement getListElementDescriptionField(WebElement listElement) {
        return listElement.findElement(By.xpath("//div"));
    }



    @Override
    public WebElement getListElementDeleteIcon(WebElement listElement) {
        return listElement.findElement(By.xpath("//h4/img"));
    }

    @Override
    public WebElement getSettingsDeleteAccountButton() {
        return driver.findElement(By.xpath("//*[@id=\"delete-account\"]/form/button"));
    }
}
