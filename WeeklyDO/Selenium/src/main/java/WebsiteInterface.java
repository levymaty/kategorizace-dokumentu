import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public interface WebsiteInterface {

     String getLogoutPage();
     String getRegisterPage();
     String getLoginPage();
     String getSettingsPage();
     String getTasksPage();
     String getListsPage();
     String getHomepage();


     //Register page elements
     WebElement getRegisterEmailField();
     WebElement getRegisterPasswordField();
     WebElement getRegisterConfirmPasswordField();
     WebElement getRegisterRegisterButton();
     WebElement getRegisterLoginButton();

    //Login page elements
    WebElement getLoginEmailField();
    WebElement getLoginPasswordField();
    WebElement getLoginLoginButton();
    WebElement getLoginRegisterButton();

    //Sidebar
    WebElement getSidebarTasksLink();
    WebElement getSidebarListsLink();
    WebElement getSidebarSettingsLink();
    WebElement getSidebarLogoutLink();

    //Tasks Topbar
    WebElement getTasksTopBarWeekBack();
    WebElement getTasksTopBarWeekForward();
    String getTasksTopBarWeekStartDate();
    String getTasksTopBarWeekEndDate();

    //Tasks
    List<WebElement> getTasksDaysForWeek();
    String getTasksTaskDate(WebElement day);
    WebElement getTasksNewTaskIcon(WebElement day);
    List<WebElement> getTasksDaysTasks(WebElement day);
    WebElement getTasksTaskTitleField(WebElement task);
    WebElement getTasksTaskDescriptionField(WebElement task);
    boolean getTasksTaskClosedStatus(WebElement task);
    WebElement getTasksTaskCloseIcon(WebElement task);
    WebElement getTasksTaskDeleteIcon(WebElement task);

    //Lists
    WebElement getListsNewListIcon();
    List<WebElement> getListsAllLists();
    WebElement getListsListNameField(WebElement list);
    WebElement getListsDeleteListIcon(WebElement list);
    WebElement getListsAddListItemIcon(WebElement list);
    List<WebElement> getListsAllListElements(WebElement list);
    WebElement getListElementTitleField(WebElement ListElement);
    WebElement getListElementDescriptionField(WebElement ListElement);
    WebElement getListElementDeleteIcon(WebElement ListElement);

    //Settings
    WebElement getSettingsDeleteAccountButton();
}
