import com.google.common.annotations.VisibleForTesting;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SeleniumEnvironment {
    WebDriver driver;

    WebsiteInterface website;


    User user;


    SeleniumEnvironment(){
        System.setProperty("webdriver.chrome.driver", "G:\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
        String homepage = "http:\\localhost";
        this.driver = new ChromeDriver();
        website = new Website(driver,homepage);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get(homepage);

        this.user = new User("abc@fmail.com", "12345678","12345678");
    }

    void logout(){
        driver.get(website.getLogoutPage());
    }

    void goToRegister(){
        driver.get(website.getRegisterPage());
    }


    void goToLogin(){
        driver.get(website.getLoginPage());

    }

    void goToSettings(){
        driver.get(website.getSettingsPage());
    }

    void goToList(){
        driver.get(website.getListsPage());
    }
    void goToTasks(){
        driver.get(website.getTasksPage());
    }

    void login(User user){
        goToLogin();
        website.getLoginEmailField().sendKeys(user.getEmail());
        website.getLoginPasswordField().sendKeys(user.getPassword());
        website.getLoginLoginButton().click();
    }

    void login(){
        goToLogin();
        website.getLoginEmailField().sendKeys(user.getEmail());
        website.getLoginPasswordField().sendKeys(user.getPassword());
        website.getLoginLoginButton().click();
    }

    void register(User user){
        goToRegister();
        website.getRegisterEmailField().sendKeys(user.getEmail());
        website.getRegisterPasswordField().sendKeys(user.getPassword());
        website.getRegisterConfirmPasswordField().sendKeys(user.getConfirmPassword());
        website.getRegisterRegisterButton().click();
    }
    void register(){
        register(this.user);
    }

    void resetEnvironmentToCleanUser(){
        login();
        goToSettings();
        website.getSettingsDeleteAccountButton().click();
        register(user);
    }

}
