import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Action;

import java.util.concurrent.TimeUnit;

public class SeleniumTests {

    SeleniumEnvironment env = new SeleniumEnvironment();

    private void carefulLogin(){
        try{
            env.login();
        }catch (Exception ignore){
            env.register();
        }
    }

    @Test
    public void registerBestCaseTest(){
        try {
            env.login();
            env.goToSettings();
            env.website.getSettingsDeleteAccountButton().click();
        }catch (Exception ignored){}
        env.register(env.user);
        Assertions.assertTrue(env.driver.getCurrentUrl().contains("localhost/tasks"));
        env.driver.close();
    }

    @Test
    public void logoutTest(){
       carefulLogin();

        env.logout();
        Assertions.assertTrue(  !env.driver.getCurrentUrl().contains("localhost/tasks") &&
                                        !env.driver.getCurrentUrl().contains("localhost/settings") &&
                                        !env.driver.getCurrentUrl().contains("localhost/register") &&
                                        !env.driver.getCurrentUrl().contains("localhost/lists"));
        env.driver.close();
    }

    @Test
    public void deleteAccountTest(){
        carefulLogin();
        env.goToSettings();
        env.website.getSettingsDeleteAccountButton().click();
        env.register(env.user);
        Assertions.assertTrue(env.driver.getCurrentUrl().contains("localhost/tasks"));
        env.driver.close();
    }

    @Test
    public void loginTests(){
        carefulLogin();
        Assertions.assertTrue(env.driver.getCurrentUrl().contains("localhost/tasks"));
        env.logout();
        env.login(new User(env.user.getEmail(), "WrongPassword","WrongPassword"));
        Assertions.assertTrue(env.driver.getCurrentUrl().contains("localhost/login"));
        env.login(new User("Unknown@email.com", env.user.getPassword(),env.user.getPassword()));
        Assertions.assertTrue(env.driver.getCurrentUrl().contains("localhost/login"));
        env.login(new User("Bad email format", env.user.getPassword(),env.user.getPassword()));
        Assertions.assertTrue(env.driver.getCurrentUrl().contains("localhost/login"));
        env.driver.close();
    }

    @Test
    public void registerBadCases(){
        try {
            env.login();
            env.goToSettings();
            env.website.getSettingsDeleteAccountButton().click();
            env.register();
            env.logout();
        }catch (Exception ignored){}
        //email taken
        env.register(env.user);
        Assertions.assertTrue(env.driver.getCurrentUrl().contains("localhost/register"));
        env.register(new User("nonRegisteredEmail@g.cz", env.user.getPassword(),"passwordsDoNotMatch"));
        Assertions.assertTrue(env.driver.getCurrentUrl().contains("localhost/register"));
        env.register(new User("Bad email format", env.user.getPassword(),env.user.getConfirmPassword()));
        Assertions.assertTrue(env.driver.getCurrentUrl().contains("localhost/register"));
        env.driver.close();
    }


    @Test
    public void taskTests(){
        //Setup
        env.resetEnvironmentToCleanUser();
        env.goToTasks();

        //First day is not the same as second
        WebElement firstDay = env.website.getTasksDaysForWeek().get(0);
        WebElement secondDay = env.website.getTasksDaysForWeek().get(1);
        Assertions.assertNotEquals(env.website.getTasksTaskDate(secondDay), env.website.getTasksTaskDate(firstDay));

        //First date shift forward works
        //TO-DO
/*
        env.website.getTasksTopBarWeekForward().click();
        firstDay = env.website.getTasksDaysForWeek().get(0);
        Assertions.assertEquals(env.website.getTasksTaskDate(secondDay), env.website.getTasksTaskDate(firstDay));

*/
        //First date shift backwards works
        //TO-DO
        /*


        env.website.getTasksTopBarWeekBack().click();
        firstDay = env.website.getTasksDaysForWeek().get(0);
        Assertions.assertNotEquals(env.website.getTasksTaskDate(secondDay), env.website.getTasksTaskDate(firstDay));
        **/

        //Adding new task works

        Assertions.assertEquals(0, env.website.getTasksDaysTasks(firstDay).size());
        env.website.getTasksNewTaskIcon(firstDay).click();
        firstDay = env.website.getTasksDaysForWeek().get(0);
        Assertions.assertEquals(1, env.website.getTasksDaysTasks(firstDay).size());

        //Removing task works
        env.website.getTasksTaskDeleteIcon(env.website.getTasksDaysTasks(firstDay).get(0)).click();
        Assertions.assertEquals(0, env.website.getTasksDaysTasks(firstDay).size());

        //Renaming task works
        env.website.getTasksNewTaskIcon(firstDay).click();
        WebElement firstTask = env.website.getTasksDaysTasks(firstDay).get(0);
        Assertions.assertEquals("Title", env.website.getTasksTaskTitleField(firstTask).getText());
        env.website.getTasksTaskTitleField(firstTask).sendKeys("Title Test");
        Assertions.assertEquals("Title", env.website.getTasksTaskTitleField(firstTask).getText());

        //Task closing works
        Assertions.assertFalse(env.website.getTasksTaskClosedStatus(firstTask));
        env.website.getTasksTaskCloseIcon(firstTask).click();
        Assertions.assertTrue(env.website.getTasksTaskClosedStatus(firstTask));
        env.website.getTasksTaskCloseIcon(firstTask).click();
        Assertions.assertFalse(env.website.getTasksTaskClosedStatus(firstTask));

        //Task description editing
        //TO-DO
        //Task moving
        /*
        Assertions.assertEquals(0, env.website.getTasksDaysTasks(secondDay).size());
        Assertions.assertEquals(1, env.website.getTasksDaysTasks(firstDay).size());
        Actions dragNDrop = new Actions(env.driver);
        dragNDrop.dragAndDropBy(firstTask,firstTask.getSize().width,0);
        dragNDrop.build();
        Assertions.assertEquals(1, env.website.getTasksDaysTasks(secondDay).size());
        Assertions.assertEquals(0, env.website.getTasksDaysTasks(firstDay).size());
        env.driver.close();
*/
    }


}
