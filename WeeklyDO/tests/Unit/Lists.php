<?php

namespace Tests\Feature;

use App\Http\Controllers\ListController;
use App\Task;
use App\TodoList;
use App\TodoListElement;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Database\Console\Factories;
use Illuminate\Support\Facades\Session;

class Lists extends TestCase
{


    /** @test
     *
     */
    public function list_is_created(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        //print(Task::all());

        $request2 = Request::create('/lists/create','POST',[
            '_token' => Session::get('_token'),
            "id" => 1,
            "name" => '',
            "user_id" => '6'
        ]);

        //$task = Task::createNewTask("2021-05-17");
        $controller = new ListController();
        $controller -> listCreate($request2);

        $this -> assertCount(1,TodoList::all());

    }

    /** @test
     *
     */
    public function list_is_updated(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $request2 = Request::create('/lists/2/update/name','POST',[
            '_token' => Session::get('_token'),
            "id" => 2,
            "name" => 'Test',
            "user_id" => '6'
        ]);

        //$task = Task::createNewTask("2021-05-17");
        $controller = new ListController();
        $controller -> listUpdateName(TodoList::first(),$request2);

        $this -> assertEquals("Test",TodoList::first() -> name);

    }

    /** @test
     *
     */
    public function list_element_is_created(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $request2 = Request::create('/lists/1/element/create','POST',[
            '_token' => Session::get('_token'),
            "id" => 5,
            "title" => "Test",
            "text" => "Testing",
            "list_id" => 1,
            "user_id" => '6'
        ]);

        //$task = Task::createNewTask("2021-05-17");
        $controller = new ListController();
        $controller -> listCreateElement(TodoList::first(),$request2);

        $this -> assertEquals("Title",TodoListElement::first() -> title);

    }

    /** @test
     *
     */
    public function list_element_title_is_updated(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $request2 = Request::create('/element/1/update/title','POST',[
            '_token' => Session::get('_token'),
            "id" => 5,
            "title" => "Test",
            "text" => "Testing",
            "list_id" => 1,
            "user_id" => '6'
        ]);

        //$task = Task::createNewTask("2021-05-17");
        $controller = new ListController();
        $controller -> listUpdateElementTitle(TodoListElement::first(),$request2);

        $this -> assertEquals("Test",TodoListElement::first() -> title);

    }

    /** @test
     *
     */
    public function list_element_text_is_updated(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $request2 = Request::create('/element/1/update/text','POST',[
            '_token' => Session::get('_token'),
            "id" => 5,
            "title" => "Test",
            "text" => "Testing",
            "list_id" => 1,
            "user_id" => '6'
        ]);

        //$task = Task::createNewTask("2021-05-17");
        $controller = new ListController();
        $controller -> listUpdateElementText(TodoListElement::first(),$request2);

        $this -> assertEquals("Testing",TodoListElement::first() -> text);

    }


    /** @test
     *
     */
    public function list_element_is_deleted(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $request2 = Request::create('/element/1/delete','POST',[
            '_token' => Session::get('_token'),
            "id" => 5,
            "title" => "Test",
            "text" => "Testing",
            "list_id" => 1,
            "user_id" => '6'
        ]);

        //$task = Task::createNewTask("2021-05-17");
        $controller = new ListController();
        $controller -> listDeleteElement(TodoListElement::first());

        $this -> assertTrue(TodoListElement::first() == null);

    }

    /** @test
     *
     */
    public function list_is_deleted(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $request2 = Request::create('/lists/2/delete','POST',[
            '_token' => Session::get('_token'),
            "id" => 2,
            "name" => 'Test',
            "user_id" => '6'
        ]);

        //$task = Task::createNewTask("2021-05-17");
        $controller = new ListController();
        $controller -> listDelete(TodoList::first());

        $this -> assertTrue(TodoList::first() == null);

    }

}
