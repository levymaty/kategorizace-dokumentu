<?php

namespace Tests\Feature;

use App\Http\Controllers\TaskController;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Support\Facades\Session;

class Lists extends TestCase
{


    /** @test
     *
     */
    public function task_is_updated_text(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $request2 = Request::create('/tasks/3/update/text','POST',[
            '_token' => Session::get('_token'),
            "id" => 3,
            "text" => 'Test',
            "user_id" => '8'
        ]);

        $controller = new TaskController();
        $controller -> updateTaskText(Task::first(),$request2);

        $this -> assertEquals("Test",Task::first() -> text);

    }

    /** @test
     *
     */
    public function task_is_updated_title(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $request2 = Request::create('/tasks/3/update/title','POST',[
            '_token' => Session::get('_token'),
            "id" => 3,
            "title" => "Test",
            "text" => 'Test',
            "user_id" => '8'
        ]);

        $controller = new TaskController();
        $controller -> updateTaskTitle(Task::first(),$request2);

        $this -> assertEquals("Test",Task::first() -> title);
    }


    /** @test
     *
     */
    public function task_is_closed(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $controller = new TaskController();
        $controller -> closeTask(Task::first());

        $this -> assertEquals(1,Task::first() -> done);

    }

    /** @test
     *
     */
    public function task_is_opened(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $controller = new TaskController();
        $controller -> openTask(Task::first());

        $this -> assertEquals(0,Task::first() -> done);

    }

    /** @test
     *
     */
    public function list_is_deleted(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $controller = new TaskController();
        $controller -> deleteTask(Task::first());

        $this -> assertTrue(Task::first() == null);

    }

}
