<?php

namespace Tests\Feature;

use App\Http\Controllers\UserController;
use App\User;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Support\Facades\Session;

class Auth extends TestCase
{

    /** @test */
    public function is_Logged_In(){
        $this -> withoutExceptionHandling();

        $user = factory(User::class) -> make();
        $this -> actingAs($user);

        $this -> assertAuthenticatedAs($user);
    }

    /** @test
     *
     */
    public function email_Is_Updated(){

        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $request2 = Request::create('/settings/update','POST',[
            '_token' => Session::get('_token'),
            "id" => 3,
            'email' => 'test123@test.com',
            "email_verified_at" => "null",
            "timezone_code" => 0,
            "created_at" => "2021-05-11T18:58:17.000000Z",
            "updated_at" => ""
        ]);

        $controller = new UserController();
        $controller -> profileUpdate($request2);

        $this -> assertSame('test123@test.com',User::first() -> email);

    }

    /** @test
     *
     */
    public function account_Is_Deleted(){
        Session::start();
        $this->be(User::all()->first());

        $this -> withoutExceptionHandling();

        $request = Request::create('/settings/delete/account','POST',[
            '_token' => Session::get('_token'),
            "id" => 1,
            'email' => 'test123@test.com',
            "email_verified_at" => "null",
            "timezone_code" => 0,
            "created_at" => "2021-05-11T18:58:17.000000Z",
            "updated_at" => ""
        ]);

        $controller = new UserController();
        $controller -> deleteAccount();
        $this -> assertTrue(User::first() == null);

    }

}
