<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ListController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::get('/logout', [LoginController::class, 'logout']);
Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::prefix('/settings')->group(function () {
    Route::get('/', [UserController::class, 'settings'])->name('settings');
    Route::post('/update', [UserController::class, 'profileUpdate'])->name('profile.update');
    Route::post('/timezone/update', [UserController::class, 'changeTimezone'])->name('timezone.update');
    Route::post('/delete/account', [UserController::class, 'deleteAccount'])->name('delete.account');
    Route::post('/change/color',  [UserController::class, 'changeColor'])->name('change.color');
    Route::post('/change/theme', [UserController::class, 'changeTheme'])->name('change.theme');
});

Route::prefix('/lists')->group(function () {
    Route::get('/', [ListController::class, 'lists'])->name('list');
    Route::post('/create', [ListController::class, 'listCreate'])->name('list.create');
    Route::post('/{list}/update/name', [ListController::class, 'listUpdateName'])->name('list.update.name');
    Route::post('/{list}/element/create', [ListController::class, 'listCreateElement'])->name('list.create.element');
    Route::post('/{list}/delete', [ListController::class, 'listDelete'])->name('list.delete');



    Route::post('/element/{listElement}/delete', [ListController::class, 'listDeleteElement'])->name('list.delete.element');
    Route::post('/element/{listElement}/update/title', [ListController::class, 'listUpdateElementTitle'])->name('list.update.element.title');
    Route::post('/element/{listElement}/update/text', [ListController::class, 'listUpdateElementText'])->name('list.update.element.text');
    Route::post('/element/{listElement}/update/list', [ListController::class, 'listUpdateElementList'])->name('list.update.element.list');
});

Route::prefix('/tasks')->group(function ()  {
    Route::get('/', [TaskController::class, 'listTasks']);

    Route::get('/{task}', [TaskController::class, 'viewTask'])->name('task');
    Route::post('/tasks', [TaskController::class, 'tasksOffset'])->name('tasks.offset');
    Route::post('/create', [TaskController::class, 'createTask'])->name('task.create');
    Route::post('/{task}/update/text', [TaskController::class, 'updateTaskText'])->name('task.update.text');
    Route::post('/{task}/update/title', [TaskController::class, 'updateTaskTitle'])->name('task.update.title');
    Route::post('/{task}/update/date', [TaskController::class, 'updateTaskDate'])->name('task.update.date');
    Route::post('/{task}/delete', [TaskController::class, 'deleteTask'])->name('task.delete');
    Route::post('/{task}/close', [TaskController::class, 'closeTask'])->name('task.close');
    Route::post('/{task}/open', [TaskController::class, 'openTask'])->name('task.open');
});
