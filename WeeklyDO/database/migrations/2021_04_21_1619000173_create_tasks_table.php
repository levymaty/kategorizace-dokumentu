<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
        $table->id();
        $table->string('title', 50)->default('Title');
		$table->string('text',255)->default('description');
		$table->boolean('done')->default(false);
		$table->date('day');
        $table->unsignedBigInteger('user_id');
		$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
