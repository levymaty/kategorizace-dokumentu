<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodolistElementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo_list_element', function (Blueprint $table) {
            $table->id();
            $table->string('title', 50)->default('Title');
            $table->string('text',255)->default('description');
            $table->unsignedBigInteger('list_id');
            $table->foreign('list_id')->references('id')->on('todo_lists')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todolist_element');
    }
}
