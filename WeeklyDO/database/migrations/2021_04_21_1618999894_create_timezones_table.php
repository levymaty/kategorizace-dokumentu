<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimezonesTable extends Migration
{
    public function up()
    {
        Schema::create('timezones', function (Blueprint $table) {
            $table->id('code');
            $table->string('country',191);
            $table->integer('timezone');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('timezones');
    }
}
