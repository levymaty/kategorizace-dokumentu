<?php


namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;
use function Symfony\Component\Translation\t;

class Timezone extends Model
{
    protected $guarded = ['id'];
    protected $table = 'timezones';

    public function create() {

    }

    public function user() {
        return $this->belongsTo(User::class, 'timezone_code', 'code');
    }
}
