<?php


namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;
use Illuminate\Support\Facades\Auth;

class Task extends Model
{
    protected $guarded = ['id'];
    protected $table = 'tasks';

    public static function createNewTask($day) {
        $day = Carbon::createFromFormat('d.m.Y', $day)->format('Y-m-d');
        return Task::create([
            'day' => $day,
            'user_id' => Auth::id()
        ]);
    }

    public function close(): bool
    {
        return $this->update([
            'done' => true
        ]);
    }

    public function open(): bool
    {
        return $this->update([
            'done' => false
        ]);
    }

    public function todoList(): Relations\BelongsTo
    {
        return $this->belongsTo(TodoList::class, 'list_tasks', 'id');
    }
}
