<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function settings() {
        return view('settings');
    }

    public function profileUpdate(Request $request) {
        Auth::user()->update([
            'email' => $request->email
        ]);
    }

    public function changeColor(Request $request) {
        $user = Auth::user();
        $user->color = $request->color;

        return $user->save();
    }

    public function changeTheme(Request $request) {
        $user = Auth::user();
        $user->theme = $request->theme;
        $user->save();

        return redirect()->route('settings');
    }

    public function changeTimezone(Request $request) {
        Auth::user()->timezone->update();

        return back()->with('success', 'Timezone changed');
    }

    public function deleteAccount() {
        $user = Auth::user();
        Auth::logout();
        $user->delete();

        return redirect()->route('home');
    }
}
