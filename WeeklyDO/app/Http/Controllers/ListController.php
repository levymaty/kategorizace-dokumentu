<?php

namespace App\Http\Controllers;

use App\Task;
use App\TodoList;
use App\TodoListElement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function lists() {
        $lists = TodoList::all();

        return view('lists', compact('lists'));
    }

    public function listCreate(Request $request) {
        return TodoList::createNewList()->id;
    }

    public function listDelete(TodoList $list) {
        return $list->delete();
    }

    public function listDeleteElement(TodoListElement $listElement) {
        return $listElement->delete();
    }

    public function listUpdateName(TodoList $list, Request $request) {
        return $list->update([
            'name' => $request->name
        ]);
    }

    public function listCreateElement(TodoList $list, Request $request) {
        return $list->createNewElement()->id;
    }

    public function listUpdateElementTitle(TodoListElement $listElement, Request $request) {
        return $listElement->update([
            'title' => $request->title
        ]);
    }

    public function listUpdateElementText(TodoListElement $listElement, Request $request) {
        return $listElement->update([
            'text' => $request->text
        ]);
    }

    public function listUpdateElementList(TodoListElement $listElement, Request $request) {
        return $listElement->update([
            'list_id' => $request->list_id
        ]);
    }
}
