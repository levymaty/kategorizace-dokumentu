<?php

namespace App\Http\Controllers;

use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listTasks() {
        $tasks = Task::all();

        $date = Carbon::now();
        $date->setISODate($date->year,$date->weekOfYear);
        $startOfWeek = $date;
        $endOfWeek = Carbon::now()->addDays(4);
        return view('tasks', compact('tasks', 'startOfWeek', 'endOfWeek'));
    }

    public function tasksOffset(Request $request) {
        $tasks = Task::all();

        if ($request->offset > 0) {
            $date = Carbon::parse($request->from)->addDays(1);
        } else {
            $date = Carbon::parse($request->from)->subDays(1);
        }
        $startOfWeek = $date->copy();
        $endOfWeek = $date->copy()->addDays(4);

        $view = view('tasks-main', compact('tasks', 'startOfWeek', 'endOfWeek'))->toHtml();

        return array( $date->copy()->format('d.m.Y'), $endOfWeek->copy()->format('d.m.Y') , $view);
    }

    public function createTask(Request $request) {
        $task = Task::createNewTask($request->day);

        return $task->id;
    }

    public function viewTask(Task $task) {
        return view('task', compact('task'));
    }

    public function updateTaskText(Task $task, Request $request) {
        return $task->update([
            'text' => $request->text
        ]);
    }

    public function updateTaskTitle(Task $task, Request $request) {
        return $task->update([
            'title' => $request->title
        ]);
    }

    public function updateTaskDate(Task $task, Request $request) {
        $day = Carbon::createFromFormat('d.m.Y', $request->date)->format('Y-m-d');
        return $task->update([
            'day' => $day
        ]);
    }

    public function deleteTask(Task $task) {
        $task->delete();

        return back()->with('success', 'Task deleted');
    }

    public function openTask(Task $task) {
        return $task->open();
    }

    public function closeTask(Task $task) {
        return $task->close();
    }
}
