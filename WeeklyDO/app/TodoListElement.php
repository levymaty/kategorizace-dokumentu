<?php


namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;
use Illuminate\Support\Facades\Auth;

class TodoListElement extends Model
{
    protected $guarded = ['id'];
    protected $table = 'todo_list_element';

    public static function createNewElement($listId) {
        return TodoListElement::create([
            'list_id' => $listId
        ]);
    }

    public function todoList(): Relations\BelongsTo
    {
        return $this->belongsTo(TodoList::class, 'id', 'list_id');
    }
}
