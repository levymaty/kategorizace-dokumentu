<?php


namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TodoList extends Model
{
    protected $guarded = ['id'];
    protected $table = 'todo_lists';

    public static function createNewList() {
        return TodoList::create([
            'name' => 'NAME',
            'user_id' => Auth::id(),
            'list_tasks' => 0
        ]);
    }

    public function createNewElement() {
        return TodoListElement::createNewElement($this->id);
    }

    public function tasks() {
    }
}
