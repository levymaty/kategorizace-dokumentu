

<div class="tile" ondrop="drop(event)" ondragover="allowDrop(event)">
    <h3><span class="date">{{$startOfWeek->format('d.m.Y')}}</span><img
            src="{{asset('assets/img/plus-square-regular.svg')}}" class="itemplus" onclick="createNewItem(event)"/></h3>
    @php
        $tasks = \App\Task::where('day', $startOfWeek->format('Y.m.d'))->get();
    @endphp
    @foreach($tasks as $task)
        <div data-wdid="{{$task->id}}" class="item @if($task->done) closed @endif" draggable="true"
             ondragstart="drag(event)">
            <h4>
                <span contenteditable="true" style="min-width: 100%;"
                      onfocusout="titleChanged(event)">{{$task->title}}</span>
                <img src="http://localhost/assets/img/check-solid.svg" class="itemplus"
                     onclick="itemCloseSignClick(event)">
                <img src="http://localhost/assets/img/trash-solid.svg" class="itemplus"
                     onclick="itemDeleteSignClick(event)">
            </h4>

            <div contenteditable="true" style="width: 100%; word-wrap:break-word;" onfocusout="contentChanged(event)">
                {{$task->text}}
            </div>
        </div>
    @endforeach
</div>
<div class="tile" ondrop="drop(event)" ondragover="allowDrop(event)">
    <h3><span class="date">{{$startOfWeek->addDays(1)->format('d.m.Y')}}</span><img
            src="{{asset('assets/img/plus-square-regular.svg')}}" class="itemplus" onclick="createNewItem(event)"/></h3>
    @php
        $tasks = \App\Task::where('day', $startOfWeek->format('Y.m.d'))->get();
    @endphp
    @foreach($tasks as $task)
        <div data-wdid="{{$task->id}}" class="item @if($task->done) closed @endif" draggable="true"
             ondragstart="drag(event)">
            <h4>
                <span contenteditable="true" style="min-width: 100%;"
                      onfocusout="titleChanged(event)">{{$task->title}}</span>
                <img src="http://localhost/assets/img/check-solid.svg" class="itemplus"
                     onclick="itemCloseSignClick(event)">
                <img src="http://localhost/assets/img/trash-solid.svg" class="itemplus"
                     onclick="itemDeleteSignClick(event)">
            </h4>

            <div contenteditable="true" style="width: 100%; word-wrap:break-word;" onfocusout="contentChanged(event)">
                {{$task->text}}
            </div>
        </div>
    @endforeach
</div>
<div class="tile" ondrop="drop(event)" ondragover="allowDrop(event)">
    <h3><span class="date">{{$startOfWeek->addDays(1)->format('d.m.Y')}}</span><img
            src="{{asset('assets/img/plus-square-regular.svg')}}" class="itemplus" onclick="createNewItem(event)"/></h3>
    @php
        $tasks = \App\Task::where('day', $startOfWeek->format('Y.m.d'))->get();
    @endphp
    @foreach($tasks as $task)
        <div data-wdid="{{$task->id}}" class="item @if($task->done) closed @endif" draggable="true"
             ondragstart="drag(event)">
            <h4>
                <span contenteditable="true" style="min-width: 100%;"
                      onfocusout="titleChanged(event)">{{$task->title}}</span>
                <img src="http://localhost/assets/img/check-solid.svg" class="itemplus"
                     onclick="itemCloseSignClick(event)">
                <img src="http://localhost/assets/img/trash-solid.svg" class="itemplus"
                     onclick="itemDeleteSignClick(event)">
            </h4>

            <div contenteditable="true" style="width: 100%; word-wrap:break-word;" onfocusout="contentChanged(event)">
                {{$task->text}}
            </div>
        </div>
    @endforeach
</div>
<div class="tile" ondrop="drop(event)" ondragover="allowDrop(event)">
    <h3><span class="date">{{$startOfWeek->addDays(1)->format('d.m.Y')}}</span><img
            src="{{asset('assets/img/plus-square-regular.svg')}}" class="itemplus" onclick="createNewItem(event)"/></h3>
    @php
        $tasks = \App\Task::where('day', $startOfWeek->format('Y.m.d'))->get();
    @endphp
    @foreach($tasks as $task)
        <div data-wdid="{{$task->id}}" class="item @if($task->done) closed @endif" draggable="true"
             ondragstart="drag(event)">
            <h4>
                <span contenteditable="true" style="min-width: 100%;"
                      onfocusout="titleChanged(event)">{{$task->title}}</span>
                <img src="http://localhost/assets/img/check-solid.svg" class="itemplus"
                     onclick="itemCloseSignClick(event)">
                <img src="http://localhost/assets/img/trash-solid.svg" class="itemplus"
                     onclick="itemDeleteSignClick(event)">
            </h4>

            <div contenteditable="true" style="width: 100%; word-wrap:break-word;" onfocusout="contentChanged(event)">
                {{$task->text}}
            </div>
        </div>
    @endforeach
</div>
<div class="tile" ondrop="drop(event)" ondragover="allowDrop(event)">
   
    <h3><span class="date">{{$startOfWeek->addDays(1)->format('d.m.Y')}}</span><img
            src="{{asset('assets/img/plus-square-regular.svg')}}" class="itemplus" onclick="createNewItem(event)"/></h3>
    @php
        $tasks = \App\Task::where('day', $startOfWeek->format('Y.m.d'))->get();
    @endphp
    @foreach($tasks as $task)
        <div data-wdid="{{$task->id}}" class="item @if($task->done) closed @endif" draggable="true"
             ondragstart="drag(event)">
            <h4>
                <span contenteditable="true" style="min-width: 100%;"
                      onfocusout="titleChanged(event)">{{$task->title}}</span>
                <img src="http://localhost/assets/img/check-solid.svg" class="itemplus"
                     onclick="itemCloseSignClick(event)">
                <img src="http://localhost/assets/img/trash-solid.svg" class="itemplus"
                     onclick="itemDeleteSignClick(event)">
            </h4>

            <div contenteditable="true" style="width: 100%; word-wrap:break-word;" onfocusout="contentChanged(event)">
                {{$task->text}}
            </div>
        </div>
    @endforeach
</div>

