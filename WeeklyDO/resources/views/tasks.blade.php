@extends('layouts.master')
@section('head')
    <style>
        .closed  {
            background-color: {{Auth::user()->color}};
        }
    </style>
@endsection
@section('body')
    <main>
        <aside class="sidebar">
            <a href="{{url('tasks')}}">Tasks</a>
            <a href="{{url('lists')}}">Lists</a>
            <a href="{{url('settings')}}">Settings</a>
            <a href="{{url('logout')}}">Log out</a>
        </aside>
        <div class="topbar"><span onclick="shiftDays(-1)"><img height="15" width="10" src="{{asset('assets/img/chevron-left-solid.svg')}}"/></span> <span
                id="first-date">{{$startOfWeek->format('d.m.Y')}}</span> - <span
                id="last-date">{{$endOfWeek->format('d.m.Y')}}</span> <span onclick="shiftDays(1)"><img height="15" width="10" src="{{asset('assets/img/chevron-right-solid.svg')}}"/></span></div>
        <div class="main-content">
            @include('tasks-main')
        </div>
    </main>
@endsection
@section('script')
    <script>
        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.target.id = "idrag";
        }

        function drop(ev) {
            ev.preventDefault();
            var el = document.getElementById("idrag");
            if(el == null) return;
            el.id = "";
            if((!ev.target.classList.contains("tile")) || (!el.classList.contains("item"))){
                return;
            }
            ev.target.appendChild(el);
            let q = ev.target.getElementsByClassName("date")[0].innerHTML;

            let url = "{{route('task.update.date', [':id'])}}";
            url = url.replace(':id', el.dataset.wdid);

            console.log(sendToServer(url, {date: q}));
        }

        function sendToServer(url, data) {
            data = data || {};
            $.extend(data, {
                '_token': '{{csrf_token()}}'
            });

            console.log(data);
            return $.ajax({
                url: url,
                type: 'POST',
                data: data
            });
        }

        function itemCloseSignClick(ev) {
            let item = ev.target.parentElement.parentElement;
            if (item.classList.contains("closed")) {
                item.classList.remove("closed");

                let url = "{{route('task.open', [':id'])}}";
                url = url.replace(':id', item.dataset.wdid);

                sendToServer(url);
            } else {
                item.classList.add("closed");

                let url = "{{route('task.close', [':id'])}}";
                url = url.replace(':id', item.dataset.wdid);

                sendToServer(url);
            }
        }

        function itemDeleteSignClick(ev) {
            let item = ev.target.parentElement.parentElement;
            item.remove();

            let url = "{{route('task.delete', [':id'])}}";
            url = url.replace(':id', item.dataset.wdid);

            sendToServer(url);
        }

        function titleChanged(ev) {
            let task = ev.target.parentElement.parentElement;

            let url = "{{route('task.update.title', [':id'])}}";
            url = url.replace(':id', task.dataset.wdid);

            sendToServer(url, {title: ev.target.innerHTML});
        }

        function contentChanged(ev) {
            let task = ev.target.parentElement;

            let url = "{{route('task.update.text', [':id'])}}";
            url = url.replace(':id', task.dataset.wdid);

            sendToServer(url, {text: ev.target.innerHTML});
        }

        async function createNewItem(ev) {
            let tile = ev.target.parentElement.parentElement;
            let item = document.createElement("div");
            let day = tile.getElementsByClassName("date")[0].innerHTML;
            let id;

            try {
                day = {
                    'day': day
                }
                id = await sendToServer("{{route('task.create')}}", day);
                console.log(id);
            } catch (err) {
                console.log(err);
            }

            item.dataset.wdid = id;
            item.className += "item";
            item.draggable = true;
            item.addEventListener("dragstart", drag, true);
            item.innerHTML =
                `<h4>
                        <span contenteditable="true" style="min-width: 100%;" onfocusout="titleChanged(event)">Title</span>
                        <img src="{{asset('assets/img/check-solid.svg')}}" class="itemplus" onclick="itemCloseSignClick(event)"/>
                        <img src="{{asset('assets/img/trash-solid.svg')}}" class="itemplus" onclick="itemDeleteSignClick(event)"/>
                    </h4>

                    <div contenteditable="true" style="width: 100%; word-wrap:break-word;" onfocusout="contentChanged(event)">
                        description
                    </div>`;
            tile.appendChild(item);
        }


        async function shiftDays(how) {
            console.log(how);
            let maincontent = "";
            try {
                let fromDate = $('#first-date').text();

                maincontent = await sendToServer("{{route('tasks.offset')}}", {from: fromDate, offset: how});
                console.log(maincontent);
                $('#first-date').text(maincontent[0]);
                $('#last-date').text(maincontent[1]);
                maincontent = maincontent[2];
            } catch (err) {
                console.log(err);
            }

            document.getElementsByClassName("main-content")[0].innerHTML = maincontent;
        }
    </script>
@endsection
