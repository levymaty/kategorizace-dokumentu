@extends('layouts.master')
@section('body')
    <main>
        <aside class="sidebar">
            <a href="{{url('tasks')}}">Tasks</a>
            <a href="{{url('lists')}}">Lists</a>
            <a href="{{url('settings')}}">Settings</a>
            <a href="{{url('logout')}}">Log out</a>
        </aside>
        <div class="topbar"></div>
        <div class="main-content">
            <img src="{{asset('assets/img/plus-square-regular.svg')}}" class="itempluslist"
                 onclick="createNewList(event)"/>
            @foreach($lists as $list)
                <div data-wdid="{{$list->id}}" class="tile" ondrop="drop(event)" ondragover="allowDrop(event)">
                    <h3><img src="http://localhost/assets/img/trash-solid.svg" class="itemplus"
                             onclick="deleteList(event)"> <span contenteditable="true" class="listname"
                                                                onfocusout="listTitleChanged(event)">{{$list->name}}</span>&nbsp;<img
                            src="{{asset('assets/img/plus-square-regular.svg')}}" class="itemplus"
                            onclick="createNewItem(event)"></h3>
                    @php
                        $elements = \App\TodoListElement::where('list_id', $list->id)->get();
                    @endphp
                    @foreach($elements as $element)
                        <div data-wdid="{{$element->id}}" class="item" draggable="true" ondragstart="drag(event)">
                            <h4>
                                <span contenteditable="true" style="min-width: 100%;"
                                      onfocusout="titleChanged(event)">{{$element->title}}</span>
                                <img src="{{asset('assets/img/trash-solid.svg')}}" class="itemplus"
                                     onclick="itemDeleteSignClick(event)">
                            </h4>

                            <div contenteditable="true" style="width: 100%; word-wrap:break-word;"
                                 onfocusout="contentChanged(event)">
                                {{$element->text}}
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </main>
@endsection
@section('script')
    <script>
        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.target.id = "idrag";
        }

        function drop(ev) {
            ev.preventDefault();
            var el = document.getElementById("idrag");
            if(el == null) return;
            el.id = "";
            if((!ev.target.classList.contains("tile")) || (!el.classList.contains("item"))){
                return;
            }

            ev.target.appendChild(el);
            let q = ev.target.dataset.wdid;


            let url = "{{route('list.update.element.list', [':id'])}}";
            url = url.replace(':id', el.dataset.wdid);

            sendToServer(url, {list_id: q});
        }

        function init() {

        }

        function itemCloseSignClick(ev) {
            let item = ev.target.parentElement.parentElement;
            if (item.classList.contains("closed")) {
                item.classList.remove("closed");
            } else {
                item.classList.add("closed");
            }
        }

        function itemDeleteSignClick(ev) {
            let item = ev.target.parentElement.parentElement;
            item.remove();
        }

        function itemDeleteSignClick(ev) {
            let item = ev.target.parentElement.parentElement;
            item.remove();

            let url = "{{route('list.delete.element', [':id'])}}";
            url = url.replace(':id', item.dataset.wdid);

            sendToServer(url);
        }

        async function createNewItem(ev) {
            let tile = ev.target.parentElement.parentElement;
            let item = document.createElement("div");

            let id;
            let message = tile.dataset.wdid;

            try {
                let url = "{{route('list.create.element', [':id'])}}";
                url = url.replace(':id', message);

                id = await sendToServer(url);
                console.log(id);
            } catch (err) {
                console.log(err);
            }

            item.dataset.wdid = id;
            item.className += "item";
            item.draggable = true;
            item.addEventListener("dragstart", drag, true);
            item.innerHTML = `
                    <h4>
                        <span contenteditable="true" style="min-width: 100%;"  onfocusout="titleChanged(event)">Title</span>
                        <img src="{{asset('assets/img/trash-solid.svg')}}" class="itemplus" onclick="itemDeleteSignClick(event)"/>
                    </h4>

                    <div contenteditable="true" style="width: 100%; word-wrap:break-word;" onfocusout="contentChanged(event)">
                        description
                    </div>`;
            tile.appendChild(item);
        }

        function sendToServer(url, data) {
            data = data || {};
            $.extend(data, {
                '_token': '{{csrf_token()}}'
            });

            console.log(data);
            return $.ajax({
                url: url,
                type: 'POST',
                data: data
            });
        }


        function listTitleChanged(ev) {
            let list = ev.target.parentElement.parentElement;
            console.log(ev.target.innerHTML);

            let url = "{{route('list.update.name', [':id'])}}";
            url = url.replace(':id', list.dataset.wdid);

            console.log(sendToServer(url, {name: ev.target.innerHTML}));
        }

        function deleteList(ev) {
            let list = ev.target.parentElement.parentElement;
            list.remove();

            let url = "{{route('list.delete', [':id'])}}";
            url = url.replace(':id', list.dataset.wdid);

            sendToServer(url);

        }

        function titleChanged(ev) {
            let listitem = ev.target.parentElement.parentElement;

            let url = "{{route('list.update.element.title', [':id'])}}";
            url = url.replace(':id', listitem.dataset.wdid);

            console.log(sendToServer(url, {title: ev.target.innerHTML}));
        }

        function contentChanged(ev) {
            let listitem = ev.target.parentElement;

            let url = "{{route('list.update.element.text', [':id'])}}";
            url = url.replace(':id', listitem.dataset.wdid);

            sendToServer(url, {text: ev.target.innerHTML});
        }

        async function createNewList(ev) {
            let maincont = ev.target.parentElement;
            let tile = document.createElement("div");
            let id;

            try {
                id = await sendToServer("{{route('list.create')}}", "");
            } catch (err) {
                console.log(err);
            }
            tile.dataset.wdid = id;
            tile.className += "tile";
            tile.addEventListener("drop", drop, true);
            tile.addEventListener("dragover", allowDrop, true);
            tile.innerHTML = `
            <h3><img src="{{asset('assets/img/trash-solid.svg')}}" class="itemplus" onclick="deleteList(event)"/> <span contenteditable="true" class="listname" onfocusout="listTitleChanged(event)">NAME</span>&nbsp<img src="{{asset('assets/img/plus-square-regular.svg')}}" class="itemplus" onclick="createNewItem(event)"/> </h3>`;
            maincont.appendChild(tile);
        }
    </script>
@endsection
