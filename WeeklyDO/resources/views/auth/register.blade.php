@extends('layouts.master')

@section('body')
    <div class="main-header-login">
        <h1>WeeklyDo</h1>
    </div>
    <main>
        <div class="main-content">
            <form method="POST" action="{{route('register')}}" class="auth-form">
                @csrf
                <label for="email">Email: </label>
                <input type="email" placeholder="Email" id="email" name="email" class="item" value="{{old('email')}}" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label for="password">Password: </label>
                <input type="password" placeholder="Password" class="item" name="password" id="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label for="password-confirm">Confirm password: </label>
                <input type="password" placeholder="Confirm" class="item" name="password_confirmation" required autocomplete="new-password">
                <input type="submit" value="Register" class="item-button">
                <input type="button" value="Login" class="item-button" onclick="window.location.href='login'">
            </form>
        </div>
    </main>
@endsection
