@extends('layouts.master')

@section('body')
    <div class="main-header-login">
        <h1>WeeklyDo</h1>
    </div>
    <main>
        <div class="main-content">
            <form method="POST" action="{{route('login')}}" class="auth-form">
                @csrf
                <label>Email: </label>
                <input type="email" placeholder="Email" class="item" name="email" id="email" value="{{old('email')}}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label>Password: </label>
                <input type="password" placeholder="Password" class="item" name="password" required autocomplete="current-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <input type="submit" class="item-button" value="Login">
                <input type="button" value="Register" class="item-button" onclick="window.location.href='register'">
            </form>
        </div>
    </main>
@endsection
