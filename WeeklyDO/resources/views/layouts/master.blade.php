<!DOCTYPE html>
<html>
<head>
    @if(\Illuminate\Support\Facades\Auth::check())
        @if(\Illuminate\Support\Facades\Auth::user()->theme == 'light')
            <link rel="stylesheet" type="text/css" href="{{asset('assets/css/styles.css')}}"/>
        @else
            <link rel="stylesheet" type="text/css" href="{{asset('assets/css/styles-dark.css')}}"/>
        @endif
    @else
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/styles.css')}}"/>
    @endif
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Antonio:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <title>WeeklyDo</title>
    @yield('head')
</head>
<body>
<div class="main-header">
    <h1>WeeklyDo</h1>
</div>
@yield('body')
@yield('script')
</body>
</html>
